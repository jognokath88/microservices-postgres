// import axios from 'axios';
import {VEHICLE_TYPE_CAR} from "../constants/vehicle_types.js";

export const routes = (router) => {
    router
        .post('/car', async (ctx) => {
            const payload = Object.assign({}, {
                vehicle_name : ctx.request.body.vehicle_name,
                color: ctx.request.body.color,
                vehicle_type : VEHICLE_TYPE_CAR
            });

            const uri = "http://backend:9000/v1/vehicle"; // in url, host is backend instead of localhost since its the name of service in docker-compose.yml
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(payload),
            }

            const response = await fetch(uri, requestOptions);

            ctx.status = response.status !== 200 ? 400 : response.status;
            
            ctx.body = response.status !== 200 ? {
                message : "Failed to reach Backend Service"
            } : await response.json();
        })
        
        .get('/car', async (ctx) => {
            const uri = 'http://backend:9000/v1/vehicle'; // in url, host is backend instead of localhost since its the name of service in docker-compose.yml
            const response = await fetch(uri);
            const data = await response.json()

            ctx.status = 200;
            ctx.body = data.reduce((accu, current) => {
                if(current.vehicle_type 
                    && current.vehicle_type === VEHICLE_TYPE_CAR && current !== undefined) {
                        accu.push(current)
                    }
                    return accu
            }, []);
        })

        
}