import Koa from 'koa';
import bodyParser from 'koa-json-body';
import Router from '@koa/router';
import logger from 'koa-logger';
import cors from '@koa/cors';

import {routes as carRoutes } from './routes/car.js';
import {routes as motorcycleRoutes } from './routes/motorycle.js';

const origin = "*";
const app = new Koa();
const router = new Router({prefix : '/v1'});

for (const route of [carRoutes, motorcycleRoutes]) {
    route(router);
}
app.use(cors({origin : origin}))
app.use(bodyParser())
app.use(logger());
app.use(router.routes());
app.use(router.allowedMethods());
app.listen(9001, console.log('Middleware is currently running at 9001'));