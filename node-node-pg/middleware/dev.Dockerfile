FROM node:18-alpine
WORKDIR /app
COPY . .
EXPOSE 9001
CMD npm run start:dev
