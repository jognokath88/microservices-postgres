import Koa from 'koa';
import bodyParser from 'koa-json-body';
import Router from 'koa-router';
import logger from 'koa-logger';
import * as swagger from 'swagger2';
import swagger2Koa from 'swagger2-koa';
import { koaSwagger } from 'koa2-swagger-ui';
import cors from '@koa/cors';
import {routes as vehicleRoutes} from './routes/vehicles.js';
import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const origin = "*";

const app = new Koa();

//swagger
const  validate  = swagger2Koa.validate; // different import, dynamic import doesn't work
// console.log('asasasa',__dirname)
const spec = swagger.loadDocumentSync(__dirname + '/swagger.yml'); // path in docker is different since it's linux ./swagger.yml won't work

if (!swagger.validateDocument(spec)) {
    throw Error ('Invalid Swagger file');
}

app.use(cors({origin: origin}));
app.use(bodyParser());
app.use(logger());

const router = new Router({prefix: "/v1"});

vehicleRoutes(router);

app.use(validate(spec));
app.use(router.routes());
app.use(router.allowedMethods());
app.use(( koaSwagger({
    routePrefix: '/docs', 
    swaggerOptions: { spec },
  })
))
app.listen(9000, console.log('backend is running at 9000'));