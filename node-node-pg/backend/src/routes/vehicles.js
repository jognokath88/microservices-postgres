// routes/vehicles.js
import { VEHICLE_CONSTANTS } from '../constants/vehicles.js';
import {getVehicle, createVehicle, getAllVehicles} from '../models/vehicles.js';
import uuid from 'uuid';

const invalid_vehicle_type = {
    status: 400,
    body: {
        message: "Invalid Vehicle Type - Please select MOTORYCLE or CAR.",
    }
}

export function routes(router) {
    router 
        .post('/vehicle', async (ctx) => {

            // error handling, if it's not right vehicle type 
            // vehicle_type must be one of any in VEHICLE_CONSTANTS
            if (!ctx.request.body.vehicle_type
                || !Object.keys(VEHICLE_CONSTANTS).includes(ctx.request.body.vehicle_type)) {
                    
                    Object.assign(ctx, ctx.request.body.vehicle_type);            
                    ctx.body = invalid_vehicle_type;

                    return;
            }
            // only need to supply vehicle_type, name, color to request
            // other fields are supplied dynamically
            const data = {
                ...ctx.request.body,
                ...VEHICLE_CONSTANTS[ctx.request.body.vehicle_type],
                plate_number: uuid(),
                created_at :  new Date().getTime(),
                updated_at:  new Date().getTime(),
            };

            const create = await createVehicle(data); // call insert query

            ctx.status = 200; 
            
            ctx.body = {
                data: create
            }

        })
        .get('/vehicle/:id', async (ctx) => {
            const vehicle_id = parseInt(ctx.params.id);
            const vehicle = await getVehicle(vehicle_id); // select vehicle with id query

            // handle errors
            if (!vehicle[0]) {
                ctx.status = 404;
                ctx.body = {
                    message: "Vehicle Not Found"
                };
                return;
            }

            ctx.status = 200;
            ctx.body = {
                ...vehicle
            };

        })
        .get('/vehicle', async (ctx) => {
            const vehicles = await getAllVehicles(); // select all query
            ctx.status = 200;
            ctx.body = vehicles;
           
        });
}