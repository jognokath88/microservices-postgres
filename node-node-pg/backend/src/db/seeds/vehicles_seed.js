export const seed = (knex, Promise) => {
    return knex('vehicles').del()
    .then(() => {
      return knex('vehicles').insert({
        created_at: new Date().getTime(),
        vehicle_type: 'car',
        vehicle_name: 'Toyota Vios',
        color: 'black',
        plate_number: 'AMG 1485',
        wheels: 4,
        engine: 'CAR_ENGINE'

      });
    })
    .then(() => {
      return knex('vehicles').insert({
        created_at: new Date().getTime(),
        vehicle_type: 'car',
        vehicle_name: 'Toyota Wigo',
        color: 'yellow',
        plate_number: 'XYZ 4911',
        wheels: 4,
        engine: 'CAR_ENGINE'
      });
    })
    .then(() => {
      return knex('vehicles').insert({
        created_at: new Date().getTime(),
        vehicle_type: 'car',
        vehicle_name: 'Toyota Fortuner',
        color: 'white',
        plate_number: 'XOL 1117',
        wheels: 4,
        engine: 'CAR_ENGINE'
      });
    });
  };