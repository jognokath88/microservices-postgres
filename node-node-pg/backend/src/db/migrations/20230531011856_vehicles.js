export const up = (knex, Promise) => {
    return knex.schema.createTable('vehicles', (table) => {
        table.bigIncrements('id').primary();
        table.bigInteger('created_at').notNullable();
        table.bigInteger('updated_at');
        table.boolean('deleted');
        table.text('vehicle_type').notNullable();
        table.text('vehicle_name').notNullable();
        table.text('color').notNullable();
        table.text('plate_number').notNullable();
        table.integer('wheels').notNullable();
        table.text('engine').notNullable();
    });
  };
  
export const down = (knex, Promise) => {
    return knex.schema.dropTable('vehicles');
};