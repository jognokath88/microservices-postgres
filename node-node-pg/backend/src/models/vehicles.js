import knex from '../postgres.js';

export async function createVehicle(data) {
    const deleted = false;
    const {
        created_at,
        updated_at,
        vehicle_type,
        vehicle_name,
        color,
        plate_number,
        wheels,
        engine
    } = data;
    const created = await knex('vehicles').insert(data).returning('*');
    // const created = await pg.value(
    //     `INSERT INTO vehicles (created_at, updated_at, deleted, vehicle_type, vehicle_name, color, plate_number, wheels, engine) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id`, created_at, updated_at, deleted, vehicle_type, vehicle_name, color, plate_number, wheels, engine 
    // );

    return created;
}

export async function getVehicle(id) {
    const vehicle = await knex('vehicles').select('*').where({id: parseInt(id)});
    if (!vehicle) {
        return null;
    }

    return vehicle;
}

export async function getAllVehicles() {
    const vehicles = await knex('vehicles').select('*');
    return vehicles;
}