import path from 'node:path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const BASE_PATH = path.join(__dirname, 'db');

export default {
  development: {
    client: 'pg',
    connection: 'postgres://postgres@postgres:5432/vehicles', // hostname is postgres since its the name of service in docker-compose.yml
    migrations: {
      directory: path.join(BASE_PATH, 'migrations')
    },
    seeds: {
      directory: path.join(BASE_PATH, 'seeds')
    }
  }
};