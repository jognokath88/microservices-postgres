# Working output

[![](./media/postgres-docker.png)](https://youtu.be/GxIDChrRtok "Working Output - postgres docker project by kate")


<iframe width="560" height="315" src="https://youtu.be/GxIDChrRtok" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<figure class="video_container">
  <iframe src="./media/postgres-docker.mkv" frameborder="0" allowfullscreen="true"> 
</iframe>
</figure>

